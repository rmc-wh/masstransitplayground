﻿using Newtonsoft.Json;

namespace Consumer1.Extensions
{
    internal static class ObjectExtensions
    {
        internal static string ToJson(this object @obj)
        {
            return JsonConvert.SerializeObject(@obj);
        }
    }
}