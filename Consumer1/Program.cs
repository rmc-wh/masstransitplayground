﻿using Consumer1.Extensions;
using IndividualManagement.Messages;
using MassTransit;
using System;

namespace Consumer1
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine(typeof(Program).AssemblyQualifiedName);

            string rabbitHost = Environment.GetEnvironmentVariable("RABBIT_HOST");
            Console.WriteLine($"RABBIT_HOST: {rabbitHost}");

            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri($"rabbitmq://{rabbitHost}"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                sbc.ReceiveEndpoint(host, "queue_ball", ep =>
                {
                    ep.Handler<IndividualEmailChangedMessage>(context =>
                    {
                        return Console.Out.WriteLineAsync($"Received IndividualEmailChangedMessage: {context.Message.ToJson()}");
                    });
                });
            });

            bus.Start(); // This is important!

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            bus.Stop();
        }
    }
}
