﻿using MassTransit;
using Messages;
using System;

namespace Consumer2
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine(typeof(Program).AssemblyQualifiedName);

            string rabbitHost = Environment.GetEnvironmentVariable("RABBIT_HOST");
            Console.WriteLine($"RABBIT_HOST: {rabbitHost}");

            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri($"rabbitmq://{rabbitHost}"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                sbc.ReceiveEndpoint(host, "test_queue", ep =>
                {
                    ep.Handler<MyMessage>(context =>
                    {
                        return Console.Out.WriteLineAsync($"Received: {context.Message.Text}");
                    });
                });
            });

            bus.Start(); // This is important!

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            bus.Stop();
        }
    }
}
