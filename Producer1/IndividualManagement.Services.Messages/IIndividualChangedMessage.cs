﻿using MassTransit;
using System;

namespace IndividualManagement.Messages
{
    public interface IIndividualChangedMessage : CorrelatedBy<Guid>
    {
        Guid IndividualId { get; }
    }
}
