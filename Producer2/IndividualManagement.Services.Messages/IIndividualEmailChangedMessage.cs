﻿using System;

namespace IndividualManagement.Messages
{
    public interface IIndividualEmailChangedMessage : IIndividualChangedMessage
    {
        string NewEmail { get; }
        string OldEmail { get; }
        DateTime TimestampUtc { get; }
    }
}