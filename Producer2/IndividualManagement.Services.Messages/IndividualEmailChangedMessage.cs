﻿using System;

namespace IndividualManagement.Messages
{
    public class IndividualEmailChangedMessage : IIndividualEmailChangedMessage
    {
        public Guid CorrelationId { get; set; } = Guid.NewGuid();

        public Guid IndividualId { get; set; }
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
        public DateTime TimestampUtc { get; set; }
    }
}
