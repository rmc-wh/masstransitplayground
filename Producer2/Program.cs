﻿using IndividualManagement.Messages;
using MassTransit;
using Messages;
using System;

namespace Producer2
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine(typeof(Program).AssemblyQualifiedName);

            string rabbitHost = Environment.GetEnvironmentVariable("RABBIT_HOST");
            Console.WriteLine($"RABBIT_HOST: {rabbitHost}");

            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri($"rabbitmq://{rabbitHost}"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });

            bus.Start(); // This is important!

            bus.Publish(new MyMessage { Text = $"Hello from {typeof(Program).AssemblyQualifiedName}" });
            bus.Publish(new IndividualEmailChangedMessage()
            {
                IndividualId = Guid.NewGuid(),
                TimestampUtc = DateTime.UtcNow,
                OldEmail = "old2@mail.com",
                NewEmail = "new2@mail.com",
            });

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            bus.Stop();
        }
    }
}
