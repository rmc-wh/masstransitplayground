param([Boolean]$Build=$true)

. .\Include.ps1

Exec { dotnet restore } "Failed to restore NuGet packages"

Write-Host "Publish-Projects"
Exec { dotnet publish -c Release -o bin/publish "Consumer1" } "Failed to publish"
Exec { dotnet publish -c Release -o bin/publish "Consumer2" } "Failed to publish"
Exec { dotnet publish -c Release -o bin/publish "Producer1" } "Failed to publish"
Exec { dotnet publish -c Release -o bin/publish "Producer2" } "Failed to publish"


# Run everything in containers
Write-Host "Running docker compose"
Invoke-Expression "docker-compose  down --remove-orphans --volumes"
Invoke-Expression "docker-compose up --force-recreate --build --abort-on-container-exit"
